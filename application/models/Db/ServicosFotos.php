<?php

class Application_Model_Db_ServicosFotos extends Zend_Db_Table
{
    protected $_name = "servicos_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Servicos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Servicos' => array(
            'columns' => 'servico_id',
            'refTableClass' => 'Application_Model_Db_Servicos',
            'refColumns'    => 'id'
        )
    );
}
