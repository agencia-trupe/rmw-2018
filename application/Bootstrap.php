<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{    
    protected function _initRoute()
    {
        $this->router = new Zend_Controller_Router_Rewrite();
		$this->request =  new Zend_Controller_Request_Http();
		$this->router->route($this->request); // pegar todos os parametros
        
        $this->bootstrap('view');
        $this->view = $this->getResource('view');
        $this->bootstrap('layout');
        $this->layout = $this->getResource('layout');
        
        if($this->request->getModuleName()=="default"){
            // dados da empresa
            $dados_empresa = new Application_Model_Db_DadosEmpresa();
            $this->view->dados_empresa = Is_Array::utf8DbRow(
                $dados_empresa->fetchRow('id = 1')
            );
            $this->view->dados = $this->view->dados_empresa;
        }
        
        if($this->request->getControllerName()=="admin"){
            $this->layout->setLayout("admin");
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME));
            
            $this->view->isLogged = $this->view->logged = $auth->hasIdentity();
            $this->view->login = $auth->hasIdentity() ? $auth->getIdentity() : null;
        }
		
        if(array_key_exists('busca',$this->request->getParams())){
            $this->view->busca = $this->request->getParam('busca');
        }
    }
    
    protected function _initLayoutConfigs(){
        $this->view->doctype('XHTML1_STRICT');
		
		switch($this->request->getControllerName()){
			case 'admin':
				$actionName = $this->request->getActionName();
				$actions = explode('/',$this->request->getRequestUri());
				$curAction = end($actions);
				$s = array_search($actionName,$actions);
				$action = $curAction!=$actions[$s]?$curAction:'index';
				//_d($action);
				$this->view->action = $action;
				$this->view->controller = $actionName;
				$this->view->module = $this->request->getControllerName();
				break;
			default:
				$this->view->action = $this->request->getActionName();
				$this->view->controller = $this->request->getControllerName();
				$this->view->module = $this->request->getModuleName();
		}
    }
    
    /**
	 * used for handling top-level navigation
	 * @return Zend_Navigation
	 */
	protected function _initNavigation()
	{
        if($this->request->getControllerName()=="admin"){   
            $login = new Zend_Session_Namespace(SITE_NAME.'_login');         
            $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation-admin.xml', 'nav');
            $config = $config->toArray();
            // _d(new Zend_Navigation($config['admin-top']));
            $this->view->menu = new stdClass();

            // lista paginas
            $pag = new Application_Model_Db_Paginas();
            $rows_pags = Is_Array::utf8DbResult($pag->fetchAll('status_id = 1',array('id')));
            
            if(count($rows_pags)){
                $config['admin-top']['paginas']['pages'] = array();

                foreach($rows_pags as $row_pag){
                    $page = array(
                        'label' => $row_pag->titulo,
                        'uri'   => URL.'/admin/paginas/edit/'.$row_pag->id
                    );
                    
                    $config['admin-top']['paginas']['pages']['pagina-'.$row_pag->alias] = $page;
                }
            }

            // ajusta links menu pagina home
            $config['admin-top']['paginas']['pages']['pagina-home'] = array(
                'label' => 'Home',
                'uri'   => '#',
                'class' => 'has-sub-sub',
                'pages' => array(
                    'banners' => array('label'=>'Banners', 'uri'=>URL.'/admin/destaques'),
                    // 'promocoes' => array('label'=>'Chamadas', 'uri'=>URL.'/admin/promocoes'),
                ),
            );
            // _d($config['admin-top']['paginas']['pages']);

            self::configUrlPrefix($config['admin-top']);
            $this->view->menu->admin_top  = new Zend_Navigation($config['admin-top']);
        } else {
            $_menu_nav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
            $menu_nav = $_menu_nav->toArray();
            $menu_nav_top = $menu_nav['top'];

            // lista servicos
            $serv = new Application_Model_Db_Servicos();
            $rows_servs = Is_Array::utf8DbResult($serv->fetchAll('status_id = 1',array('ordem')));
            
            if(count($rows_servs)){
                $menu_nav_top['servicos']['pages'] = array();

                foreach($rows_servs as $row_serv){
                    $page = array(
                        'label' => $row_serv->titulo,
                        'uri'   => URL.'/servicos/'.$row_serv->alias.'-'.$row_serv->id
                    );
                    
                    $menu_nav_top['servicos']['pages']['servico-'.$row_serv->alias] = $page;
                }
            }
            // _d($menu_nav_top);

            // prefixos
            self::configUrlPrefix($menu_nav_top);
            // self::configUrlPrefix($menu_nav['footer']);
			
            $this->view->menu = new stdClass();
            $this->view->menu->top        = new Zend_Navigation($menu_nav_top);
            // $this->view->menu->footer    = new Zend_Navigation($menu_nav['footer']);
            
            // $menuFooter = Filler::menuFooter();
            $menuFooter = array(array(),array());
            $this->servicos = new Application_Model_Db_Servicos();
            $servs = $this->servicos->fetchAllWithPhoto('status_id=1',array('ordem','titulo'));
            $servsMid = count($servs)/2; $i=0;
            foreach($servs as $serv) { $i++;
                $s = (array)$serv;
                $s['label'] = $serv->titulo;
                $s['uri'] = URL.'/servicos';
                
                ($i<=$servsMid) ?
                    $menuFooter[0][] = $s :
                    $menuFooter[1][] = $s;
            }
            // _d($menuFooter);
            $this->view->menu->footer1 = new Zend_Navigation($menuFooter[0]);
            $this->view->menu->footer2 = new Zend_Navigation($menuFooter[1]);

			
            $uri = APPLICATION_ENV == 'development' ?
                    URL.$this->request->getPathInfo() : // dev
                    URL.$this->request->getPathInfo();  // production
            
            foreach(get_object_vars($this->view->menu) as $menu){
                $activeNav = $menu->findByUri($uri) or
                $activeNav = $menu->findByUri(str_replace('http://'.$_SERVER['HTTP_HOST'],'',$uri));
                
                if(null !== $activeNav){
                    $activeNav->active = true;
                    $activeNav->setClass($activeNav->getClass()." active");	
                }
            }
        }
	}

    function configUrlPrefix(&$config)
    {
        foreach($config as &$c){
            // adiciona url ao link
            if(isset($c['uri'])) if($c['uri'] != '#' & !strstr($c['uri'],'http')) $c['uri'] = URL.$c['uri'];
            // adiciona recursão à função
            if(isset($c['pages'])) $c['pages'] = self::configUrlPrefix($c['pages']);
        }
        
        return $config;
    }
}