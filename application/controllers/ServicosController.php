<?php

class ServicosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->servicos = new Application_Model_Db_Servicos();
        $this->paginas = new Application_Model_Db_Paginas();
        // $this->view->titulo = 'Serviços';
    }

    public function indexAction()
    {
    	$pagina = $this->paginas->getPagina(4);
        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->titulo2;

        $alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
        if($alias) {
        	$id = end(explode('-', $alias));
	        $row = $this->servicos->fetchRowWithPhoto('t1.id="'.$id.'"',array('ordem','titulo'));
	        $this->view->row = $row;
        	// _d($row);
        	return;
        }
        
        $rows = $this->servicos->fetchAllWithPhoto('status_id=1',array('ordem','titulo'));
        $this->view->rows = $rows;
        // _d($rows);
    }


}

