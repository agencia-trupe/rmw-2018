var docHeight = $(document).height(), 
	viewPortWidth,
	viewPortHeight,
	bannerHeight = 0,
	bhPd = 211; // 250

getViewport();
// bannerHeight = docHeight - 250;
bannerHeight = viewPortHeight - bhPd;
bannerHeights();

$(window).resize(function(e){
    getViewport();
	bannerHeight = viewPortHeight - bhPd;
	bannerHeights();
});

head.js(JS_PATH+"/cycle.min.js",function(){
	$('#slideshow').before('<ul id="banner-nav">').cycle({
		slideResize: true,
		containerResize: true,
		width: '100%',
		height: '100%',
		fit: 1,
		fx:'fade',
		speed:2000,
		// timeout: 0,
		// speedIn:4000,
		// speedOut:100,
		pager:"#banner-nav"
	});
});

function bannerHeights() {
	if(!isSmartphone || 0){
		$('#slideshow-wrapper').height(bannerHeight);
		$('#slideshow .foto').height(bannerHeight);
		$('#slideshow .foto .img').height(bannerHeight);
	}
}

function getViewport() {
 // var viewPortWidth;
 // var viewPortHeight;

 // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
 if (typeof window.innerWidth != 'undefined') {
   viewPortWidth = window.innerWidth,
   viewPortHeight = window.innerHeight
 }

// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
 else if (typeof document.documentElement != 'undefined'
 && typeof document.documentElement.clientWidth !=
 'undefined' && document.documentElement.clientWidth != 0) {
    viewPortWidth = document.documentElement.clientWidth,
    viewPortHeight = document.documentElement.clientHeight
 }

 // older versions of IE
 else {
   viewPortWidth = document.getElementsByTagName('body')[0].clientWidth,
   viewPortHeight = document.getElementsByTagName('body')[0].clientHeight
 }
 // return [viewPortWidth, viewPortHeight];
}
