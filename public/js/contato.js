$(document).ready(function(){
    _prevalues();
    
    $('input#telefone').focus(function(){
        $(this).removeClass('pre');
    }).blur(function(){
        var $t = $(this);
        if($t.val() == ''){
            $t.addClass('pre');
        }
    });
    
    // enviando contato
    head.js(JS_PATH+'/is.simple.validate.js',function(){
        $('a#submit, #fale-enviar').click(function(){
            $('form.contato.fale').submit();
        });
        $('a#submit-orc, #orc-enviar').click(function(){
            $('form.contato.orc').submit();
        });
        
        $('form.contato1').submit(function(e){
            e.preventDefault();
            var $this = $(this),
                $status = $this.find('p.status').removeClass('error');
            $status.html('Enviando...');
            
            if($(this).isSimpleValidate({pre:true})){
                var url  = this.action+'.json',
                    data = $this.serialize();
                
                $.post(url,data,function(json){
                    if(json.error){
                        $status.addClass('error').html(json.error);
                        return false;
                    }
                    
                    // $status.html(json.msg);
                    $status.html(json.msg);
                    _resetForm(this);
                },'json');
            } else {
                $status.addClass('error').html('* Preencha todos os campos');
            }

            return false;
        });
        
        $('form.contato').submit(function(e){
            e.preventDefault();
            var $this = $(this),
                $status = $this.find('p.status').html('').removeClass('error'),
                $txts = $this.find('.txt'),
                $txtVisible = $this.find('.txt:visible'),
                $load = $this.find('.load'),
                $txtNext = $txtVisible.next(),
                invalids = [],
                loads = 0, totalLoads = $txts.size(), loadPct = 0,
                $elmErr, elmErr;
            
            if($txtVisible.val()=='' ||
               $txtVisible.val()=='(__)____-_____'){
                $status.addClass('error').html($txtVisible.attr('placeholder').replace(':',' corretamente'));
                return false;
            }

            $txts.each(function(i,elm){
                if(elm.value=='') invalids.push(elm.id);
                else loads++;
            });
            loadPct = (loads / totalLoads) * 100;
            // console.log(loads+' de '+totalLoads+' = '+loadPct);
            $load.css('width',loadPct+'%');
            
            $txtNext.focus();
            $txts.fadeOut();
            $txtNext.focus().fadeIn().focus();
            if($txtNext.hasClass('txt-enviar')){
                _sto(function() {
                    $txtNext.addClass('visible');
                    $this.removeClass('incomplete');
                    $this.addClass('complete');
                    $this.find('.submit').focus();
                },.5);
                return false;
            } else if($this.hasClass('incomplete')) {
                // _sto(function() {
                //     $txtNext.fadeIn().focus();
                // },.3);
                return false;
            }

            if(!$this.hasClass('complete')||$this.hasClass('incomplete')){
                $this.find('.txt-enviar').removeClass('visible');
                $status.addClass('error').html('* Preencha todos os campos');
                return false;
            }
            
            if(invalids.length) {
                // $elmErr = $('#'+invalids[0]);
                $this.find('.txt-enviar').removeClass('visible');
                elmErr = document.getElementById(invalids[0]);
                $status.addClass('error').html(elmErr.placeholder.replace(':',' corretamente'));
                $(elmErr).focus().fadeIn().focus();
                _sto(function() {
                    $(elmErr).focus();//.fadeIn().focus();
                },.5);
                return false;
            }


            $status.html('Enviando...');
            
            if($(this).isSimpleValidate({pre:true})){
                var url  = this.action+'.json',
                    data = $this.serialize();
                
                $.post(url,data,function(json){
                    if(json.error){
                        $status.addClass('error').html(json.error);
                        return false;
                    }
                    
                    // $status.html(json.msg);
                    $status.html(json.msg);
                    _resetForm($this);
                    $this.find('.txt-enviar').removeClass('visible');
                    $this.find('.txt-nome').fadeIn();
                    $load.css('width','0%');
                },'json');
            } else {
                $status.addClass('error').html('* Preencha todos os campos');
            }
            $this.addClass('incomplete').removeClass('complete');

            return false;
        });
    });
});

function _prevalues(v){
    $('.prevalue').each(function(){
        var $t = $(this);
        $t.prevalue(v || $t.data('prevalue'));
    });
}

function _resetForm(formSelector){
    $(formSelector).find('input,textarea').val('');
    $(formSelector).find('select option:first-child').attr('selected',true);
}